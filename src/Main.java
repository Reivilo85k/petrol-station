import java.math.BigDecimal;
import java.util.Scanner;

//Petrol Station
//
//
//Simulate the process of refueling. Within the while loop ask user if you should
//continue or finish. For every entered “continue” command you should add a specific
//amount of petrol and money (both of type double) and view it on the console.
//At the end user should pay for petrol. Consider multiple possibilities, like:
//
//The user paid exactly as much as required.
//The user paid too much (cashier should return the rest of the money).
//The user paid too little – should be asked for the rest.

public class Main {
    public static void main(String[] args) {
        double totalCost = 0;
        totalCost = Fuel.Choice(totalCost);
        BigDecimal bd = new BigDecimal(totalCost);
        bd= bd.setScale(2,BigDecimal.ROUND_DOWN);
        totalCost = bd.doubleValue();
        Payment(totalCost);

    }

    private static void Payment(double totalCost){
        Scanner pay = new Scanner(System.in);
        boolean transactionComplete = false;
        double payment;

        while (!transactionComplete) {
            System.out.printf("\nThe total cost is %.2f", totalCost);
            System.out.println("\nPlease input the specified amount for payment");
            payment = pay.nextDouble();
            if (payment == totalCost) {
                System.out.println("\nThank you for your purchase ! Drive Safe !\n\n\n");
                transactionComplete = true;
            } else if (payment > totalCost) {
                System.out.println("Your payment has exceeded the cost of your purchase. Please wait for a member of staff");
                payment -= totalCost;
                System.out.printf("\nThe cashier refunds %.2f",payment);
                System.out.println("\nThank you for your purchase ! Drive Safe !\n\n\n");
                transactionComplete = true;
            } else if (payment < totalCost) {
                totalCost -= payment;
            }
        }
    }
}
