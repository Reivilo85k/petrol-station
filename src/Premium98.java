public class Premium98 extends Fuel{
    private int fuel;
    private double money = 1.199;

    @Override
    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    @Override
    public int getFuel() {
        return fuel;
    }

    @Override
    public double getMoney() {
        return money;
    }

}
