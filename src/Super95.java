public class Super95 extends Fuel{
    private int fuel;
    private double money = 1.149;

    @Override
    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    @Override
    public int getFuel() {
        return fuel;
    }

    @Override
    public double getMoney() {
        return money;
    }
}
