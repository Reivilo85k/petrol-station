import java.util.Scanner;

public class Fuel {
    private int fuel;
    private double money;

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money){
        this.money = money;
    }

    public static double Choice(double totalCost){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to Texaco \nPlease chose fuel type \n1- Super 95 \n2- Premium 98 \n3-Diesel \n4-LPG");
        System.out.println("\n------------------------------------------------------------------\nFuel type : ");
        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                System.out.println("\nYou have selected Super 95");
                Super95 super95 = new Super95();
                totalCost = super95.refuel(totalCost, super95.getFuel(), super95.getMoney());
                break;
            case 2:
                System.out.println("\nYou have selected Premium 98");
                Premium98 premium98 = new Premium98();
                totalCost = premium98.refuel(totalCost, premium98.getFuel(), premium98.getMoney());
                break;
            case 3:
                System.out.println("\nYou have selected Diesel");
                Diesel diesel = new Diesel();
                totalCost = diesel.refuel(totalCost, diesel.getFuel(), diesel.getMoney());
                break;
            case 4:
                System.out.println("\nYou have selected LPG");
                LPG lpg = new LPG();
                totalCost = lpg.refuel(totalCost, lpg.getFuel(), lpg.getMoney());
                break;
            default:
                System.out.println("\nThere was a problem with your selection.\nPlease try again or contact staff.");
        }return totalCost;
    }

    public static double refuel(double totalCost, int fuel, double money){
        boolean loop = true;
        int fuelTrack = 0;
        Scanner input = new Scanner(System.in);
        while (loop) {
            System.out.println("Please input how many liters of fuel");
            fuel = input.nextInt();
            totalCost += (fuel * money);
            fuelTrack += fuel;
            System.out.printf("\nYou have used %d liters of fuel and your total cost is %.2f",fuelTrack, totalCost);
            System.out.println("\nDo you want to continue Y/N\n");
            String in = input.next();
            if (in.equals("Y") || in.equals("y") || in.equals("YES") || in.equals("yes") || in.equals("Yes")) {
                loop = true;
            } else {
                loop = false;
                System.out.printf("\nYou have used %d liters of fuel", fuelTrack);
            }
        }return totalCost;
    }
}
