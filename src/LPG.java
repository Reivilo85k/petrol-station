public class LPG extends Fuel{
    private int fuel;
    private double money = 0.569;

    @Override
    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    @Override
    public int getFuel() {
        return fuel;
    }

    @Override
    public double getMoney() {
        return money;
    }
}
